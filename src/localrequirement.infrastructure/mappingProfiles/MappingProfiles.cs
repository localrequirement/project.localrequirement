﻿using localrequirement.application.dtos;
using localrequirement.domain.Entities;

namespace localrequirement.infrastructure.mappingProfiles
{
    internal class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<StudentDto, Student>();
            CreateMap<Student, StudentDto>();
        }
    }
}
