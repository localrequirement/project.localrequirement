﻿using localrequirement.application.dtos;
using localrequirement.application.interfaces;
using localrequirement.domain.entities.identity;
using localrequirement.infrastructure.data;
using Microsoft.AspNetCore.Identity;

namespace localrequirement.infrastructure.services
{
    internal class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly IdentityAppDbContext _db;
        private readonly IMapper _mapper;

        public UserService(UserManager<User> userManager, IdentityAppDbContext db, IMapper mapper)
        {
            this._userManager = userManager;
            this._db = db;
            this._mapper = mapper;
        }

        public RefreshTokenDto AddUserRefreshTokens(RefreshTokenDto payload)
        {
            var _payload = _mapper.Map<RefreshToken>(payload);
            _db.RefreshTokens.Add(_payload);
            return payload;
        }

        public void DeleteUserRefreshTokens(string userId, string refreshToken)
        {
            var _refreshToken = _db.Users.FirstOrDefault(t => t.Id == userId)?
                .RefreshTokens.FirstOrDefault(t=>t.Id==new Guid( refreshToken));
            if (_refreshToken != null)
                _db.RefreshTokens.Remove(_refreshToken);
        }

        public RefreshTokenDto GetRefreshTokens(string userId, string refreshToken)
        {
            var _refreshToken = _db.Users.FirstOrDefault(t => t.Id == userId)?
                .RefreshTokens.FirstOrDefault(t => t.Id == new Guid(refreshToken));
            return _mapper.Map<RefreshTokenDto>(_refreshToken);
        }

        public async Task<bool> IsValidUserAsync(UserLoginDto user)
        {
            var u = _userManager.Users.FirstOrDefault(o => o.UserName == user.Username);
            var result = await _userManager.CheckPasswordAsync(u, user.Password);
            return result;
        }
        public int SaveCommit()
        {
            return _db.SaveChanges();
        }

       
    }
}
