﻿using localrequirement.application.interfaces;
using Newtonsoft.Json;

namespace localrequirement.infrastructure.services
{
    internal class NewtonsoftJsonSerializerService : IJsonSerializerService
    {
        public async Task<T> DeserializeAsync<T>(string payload)
        {
            if (string.IsNullOrEmpty(payload))
                return await Task.FromResult(default(T));
            return await Task.FromResult(JsonConvert.DeserializeObject<T>(payload));
        }

        public async Task<string> SerializeAsync<T>(T payload)
        {
            return await Task.FromResult( JsonConvert.SerializeObject(payload));
        }
    }
}
