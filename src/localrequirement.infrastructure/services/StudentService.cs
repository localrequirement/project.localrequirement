﻿using localrequirement.application.dtos;
using localrequirement.application.interfaces;
using localrequirement.domain.Entities;
using localrequirement.infrastructure.data;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace localrequirement.infrastructure.services
{
    internal class StudentService : IStudentServices
    {
        private readonly AppDbContext _dbContext;
        private readonly IMapper _mapper;

        public StudentService(AppDbContext dbContext,IMapper mapper)
        {
            this._dbContext = dbContext;
            this._mapper = mapper;
        }
        public async Task<StudentDto> AddAsync(StudentDto payload)
        {
            var _payload = _mapper.Map<Student>(payload);
            await _dbContext
                .Students
                .AddAsync(_payload);
            _dbContext.SaveChanges();
            return _mapper.Map<StudentDto>(_payload);
        }

        public async Task<IEnumerable<StudentDto>> GetAllAsync()
        {
            var students = await _dbContext.Students.ToListAsync();
            return _mapper.Map<IEnumerable<StudentDto>>(students);

        }

        public async Task<bool> UpdateAsync(StudentDto payload)
        {
            var student = new Student
            {
                Id = payload.Id,
                Name = payload.Name
            };
            _dbContext.Update(student);
            _dbContext.SaveChanges();
            return await Task.FromResult(true);
        }
    }
}
