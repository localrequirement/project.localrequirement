﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace localrequirement.infrastructure
{
    internal class Constants
    {
        internal const string DB_CONSTRING= "DefaultConnection";
        internal const string ASPNETCORE_ENVIRONMENT = "ASPNETCORE_ENVIRONMENT";
        internal const string STARTUP_PROJECT_PATH = "../localrequirement.api";
    }
}
