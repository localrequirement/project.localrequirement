global using localrequirement.infrastructure;
global using localrequirement.infrastructure.services;
global using localrequirement.infrastructure.mappingProfiles;
global using System;
global using AutoMapper;