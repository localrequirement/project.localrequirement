﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Diagnostics;

namespace localrequirement.infrastructure.data
{
    internal class DesignTimeDbContextFactory_AppDbContext :
        IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            string environment = Environment.GetEnvironmentVariable(Constants.ASPNETCORE_ENVIRONMENT);
            System.Diagnostics.Debug.WriteLine(Directory.GetCurrentDirectory());
            var configDirectory = Path.Combine(Directory.GetCurrentDirectory(), Constants.STARTUP_PROJECT_PATH);
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(configDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            var connectionString = configuration.GetConnectionString(Constants.DB_CONSTRING);
            builder.UseMySQL(connectionString);
            return new AppDbContext(builder.Options);
        }
    }
    internal class DesignTimeDbContextFactory_IdentityAppDbContext :
        IDesignTimeDbContextFactory<IdentityAppDbContext>
    {
        public IdentityAppDbContext CreateDbContext(string[] args)
        {
            string environment = Environment.GetEnvironmentVariable(Constants.ASPNETCORE_ENVIRONMENT);
            System.Diagnostics.Debug.WriteLine(Directory.GetCurrentDirectory());
            var configDirectory = Path.Combine(Directory.GetCurrentDirectory(), Constants.STARTUP_PROJECT_PATH);
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(configDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
            var builder = new DbContextOptionsBuilder<IdentityAppDbContext>();
            var connectionString = configuration.GetConnectionString(Constants.DB_CONSTRING);
            builder.UseMySQL(connectionString);
            return new IdentityAppDbContext(builder.Options);
        }
    }
}
