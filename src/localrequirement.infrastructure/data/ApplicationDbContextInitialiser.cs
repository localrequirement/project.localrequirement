﻿using localrequirement.domain.entities.identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace localrequirement.infrastructure.data;
internal class AppDbContextInitialiser_IdentityAppDbContext
{
    private readonly ILogger<AppDbContextInitialiser_IdentityAppDbContext> _logger;
    private readonly IdentityAppDbContext _context;
    private readonly UserManager<User> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;

    public AppDbContextInitialiser_IdentityAppDbContext(ILogger<AppDbContextInitialiser_IdentityAppDbContext> logger,
        IdentityAppDbContext context,
        UserManager<User> userManager, RoleManager<IdentityRole> roleManage)
    {
        _logger = logger;
        _context = context;
        this._userManager = userManager;
        this._roleManager = roleManage;
    }

    public async Task InitialiseAsync()
    {
        try
        {
            await _context.Database.MigrateAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while initialising the database.");
            throw;
        }
    }

    public async Task SeedAsync()
    {
        try
        {
            _logger.LogInformation("database seed started..");
            await SeedRolesAsync();
            _logger.LogInformation("Role seed done.");
            await SeedUsersAsync();
            _logger.LogInformation("database seed completed successfully..");

        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while seeding the database.");
            throw;
        }
    }

    private async Task SeedRolesAsync()
    {
        var id_Anonymous = Convert.ToString((int)domain.enums.Roles.Anonymous);
        var id_SuperAdmin = Convert.ToString((int)domain.enums.Roles.SuperAdmin);
        var id_Customar = Convert.ToString((int)domain.enums.Roles.Customar);
        var id_ServiceProvider = Convert.ToString((int)domain.enums.Roles.ServiceProvider);

        //Seed Roles
        if (!(await _roleManager.RoleExistsAsync(domain.enums.Roles.Anonymous.ToString())))
            await _roleManager.CreateAsync(new IdentityRole(domain.enums.Roles.Anonymous.ToString()) { Id = id_Anonymous });
        if (!(await _roleManager.RoleExistsAsync(domain.enums.Roles.SuperAdmin.ToString())))
            await _roleManager.CreateAsync(new IdentityRole(domain.enums.Roles.SuperAdmin.ToString()) { Id = id_SuperAdmin });
        if (!(await _roleManager.RoleExistsAsync(domain.enums.Roles.Customar.ToString())))
            await _roleManager.CreateAsync(new IdentityRole(domain.enums.Roles.Customar.ToString()) { Id = id_Customar });
        if (!(await _roleManager.RoleExistsAsync(domain.enums.Roles.ServiceProvider.ToString())))
            await _roleManager.CreateAsync(new IdentityRole(domain.enums.Roles.ServiceProvider.ToString()) { Id = id_ServiceProvider });
        _context.SaveChanges();
    }
    private async Task SeedUsersAsync()
    {
        //anonymous
        var user0 = new User
        {
            Id = "0",
            UserName = "anonymous",
            Email = "anonymous@default.com",
            Name = "Anonymous",
            EmailConfirmed = true,
            PhoneNumberConfirmed = true,
            Password = null,
            LockoutEnabled = true,
            IsAnonymous = true
        };
        // superadmin
        var user1 = new User
        {
            Id = "1",
            UserName = "superadmin",
            Email = "superadmin@gmail.com",
            Name = "Super Admin",
            EmailConfirmed = true,
            PhoneNumberConfirmed = true,
            Password = "123Pa$$word."
        };
        //customar 1
        var user2 = new User
        {
            Id = "100",
            UserName = "customar1",
            Email = "customar1@default.com",
            Name = "customar1",
            EmailConfirmed = true,
            PhoneNumberConfirmed = true,
            Password = "123Pa$$word.",
            LockoutEnabled = true,
        };
        //customar 2
        var user3 = new User
        {
            Id = "101",
            UserName = "customar2",
            Email = "customar2@default.com",
            Name = "customar2",
            EmailConfirmed = true,
            PhoneNumberConfirmed = true,
            Password = "123Pa$$word.",
            LockoutEnabled = true,
        };
        //customar 2
        var user4 = new User
        {
            Id = "102",
            UserName = "sp1",
            Email = "sp1@default.com",
            Name = "sp1",
            EmailConfirmed = true,
            PhoneNumberConfirmed = true,
            Password = "123Pa$$word.",
            LockoutEnabled = true,
        };
        if ((await _userManager.FindByIdAsync(user0.Id)) == null)
            await _userManager.CreateAsync(user0);
        if ((await _userManager.FindByIdAsync(user1.Id)) == null)
            await _userManager.CreateAsync(user1, user1.Password);
        if ((await _userManager.FindByIdAsync(user2.Id)) == null)
            await _userManager.CreateAsync(user2, user2.Password);
        if ((await _userManager.FindByIdAsync(user3.Id)) == null)
            await _userManager.CreateAsync(user3, user3.Password);
        if ((await _userManager.FindByIdAsync(user4.Id)) == null)
            await _userManager.CreateAsync(user4, user4.Password);

        await _context.SaveChangesAsync();


        // add role to anonymous
        user0 = await _userManager.FindByIdAsync(user0.Id);
        if (!(await _userManager.IsInRoleAsync(user0, domain.enums.Roles.Anonymous.ToString())))
            await _userManager.AddToRoleAsync(user0, domain.enums.Roles.Anonymous.ToString());

        // add role to SuperAdmin
        user1 = await _userManager.FindByIdAsync(user1.Id);
        if (!(await _userManager.IsInRoleAsync(user1, domain.enums.Roles.ServiceProvider.ToString())))
            await _userManager.AddToRoleAsync(user1, domain.enums.Roles.ServiceProvider.ToString());
        if (!(await _userManager.IsInRoleAsync(user1, domain.enums.Roles.Anonymous.ToString())))
            await _userManager.AddToRoleAsync(user1, domain.enums.Roles.Anonymous.ToString());
        if (!(await _userManager.IsInRoleAsync(user1, domain.enums.Roles.Customar.ToString())))
            await _userManager.AddToRoleAsync(user1, domain.enums.Roles.Customar.ToString());
        if (!(await _userManager.IsInRoleAsync(user1, domain.enums.Roles.SuperAdmin.ToString())))
            await _userManager.AddToRoleAsync(user1, domain.enums.Roles.SuperAdmin.ToString());

        // add role to Customar: user2
        user2 = await _userManager.FindByIdAsync(user2.Id);
        if (!(await _userManager.IsInRoleAsync(user2, domain.enums.Roles.Anonymous.ToString())))
            await _userManager.AddToRoleAsync(user2, domain.enums.Roles.Anonymous.ToString());
        if (!(await _userManager.IsInRoleAsync(user2, domain.enums.Roles.Customar.ToString())))
            await _userManager.AddToRoleAsync(user2, domain.enums.Roles.Customar.ToString());

        // add role to Customar: user3
        user3 = await _userManager.FindByIdAsync(user3.Id);
        if (!(await _userManager.IsInRoleAsync(user3, domain.enums.Roles.Anonymous.ToString())))
            await _userManager.AddToRoleAsync(user3, domain.enums.Roles.Anonymous.ToString());
        if (!(await _userManager.IsInRoleAsync(user3, domain.enums.Roles.Customar.ToString())))
            await _userManager.AddToRoleAsync(user3, domain.enums.Roles.Customar.ToString());

        // add role to Service Provider: user4
        user4 = await _userManager.FindByIdAsync(user4.Id);
        if (!(await _userManager.IsInRoleAsync(user4, domain.enums.Roles.Anonymous.ToString())))
            await _userManager.AddToRoleAsync(user4, domain.enums.Roles.Anonymous.ToString());
        if (!(await _userManager.IsInRoleAsync(user4, domain.enums.Roles.ServiceProvider.ToString())))
            await _userManager.AddToRoleAsync(user4, domain.enums.Roles.ServiceProvider.ToString());

        await _context.SaveChangesAsync();
        _logger.LogInformation("User seed done.");
    }
}
