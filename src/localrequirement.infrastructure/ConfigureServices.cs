using GuardNet;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using localrequirement.infrastructure.data;
using localrequirement.application.interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using localrequirement.domain.entities.identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace localrequirement.infrastructure;
public static class ConfigureServices
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString(Constants.DB_CONSTRING);
        Guard.NotNullOrWhitespace(connectionString, Constants.DB_CONSTRING, message: $"Connection string '{Constants.DB_CONSTRING}' not found.");

        services.AddDbContext<data.AppDbContext>((sp, options) =>
        {
            options.AddInterceptors(sp.GetServices<ISaveChangesInterceptor>());
            options.UseMySQL(connectionString);
        });

        services.AddScoped<AppDbContextInitialiser_IdentityAppDbContext>();
        services.AddAutoMapper(typeof(ConfigureServices).Assembly);
        services.AddTransient<IStudentServices, StudentService>();

        // json converter
        JsonConvert.DefaultSettings = () => new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };
        services.AddSingleton<IJsonSerializerService, NewtonsoftJsonSerializerService>();
        return services;
    }
    public static async Task InitialiseDatabaseAsync(this IServiceProvider app)
    {
        using (var scope = app.CreateScope())
        {
            var initialiser = scope.ServiceProvider.GetRequiredService<AppDbContextInitialiser_IdentityAppDbContext>();
            await initialiser.InitialiseAsync();
            await initialiser.SeedAsync();
        }

    }
    public static IServiceCollection AddAuthentication(this IServiceCollection services, IConfiguration _configuration)
    {
        var connectionString = _configuration.GetConnectionString(Constants.DB_CONSTRING);
        Guard.NotNullOrWhitespace(connectionString, Constants.DB_CONSTRING, message: $"Connection string '{Constants.DB_CONSTRING}' not found.");

        services.AddDbContext<IdentityAppDbContext>(options => { 
            options.UseMySQL(connectionString);
            options.EnableSensitiveDataLogging();
            //options.UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole()));
        });
        services.AddIdentity<User, IdentityRole>(options => {
            options.Password.RequireUppercase = true; // on production add more secured options
            options.Password.RequireDigit = true;
            options.SignIn.RequireConfirmedEmail = true;
        }).AddEntityFrameworkStores<IdentityAppDbContext>().AddDefaultTokenProviders();

        services.AddAuthentication(x => {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(o => {
            var Key = Encoding.UTF8.GetBytes(_configuration["JWT:Key"]);
            o.SaveToken = true;
            o.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false, // on production make it true
                ValidateAudience = false, // on production make it true
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = _configuration["JWT:Issuer"],
                ValidAudience = _configuration["JWT:Audience"],
                IssuerSigningKey = new SymmetricSecurityKey(Key),
                ClockSkew = TimeSpan.Zero
            };
            o.Events = new JwtBearerEvents
            {
                OnAuthenticationFailed = context => {
                    if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                    {
                        context.Response.Headers.Add("IS-TOKEN-EXPIRED", "true");
                    }
                    return Task.CompletedTask;
                }
            };
        });

        services.AddScoped<IUserStore<User>>(t =>
        {
            return new UserStore<User>(t.GetService<IdentityAppDbContext>())
            {
                AutoSaveChanges = false
            };
        });
        services.AddScoped<IRoleStore<IdentityRole>>(t =>
        {
            return new RoleStore<IdentityRole>(t.GetService<IdentityAppDbContext>())
            {
                AutoSaveChanges = false
            };
        });
        services.AddSingleton<IJWTService, JWTServices>();
        services.AddScoped<IUserService, UserService>();
        return services;
    }

}
