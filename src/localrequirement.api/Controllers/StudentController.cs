﻿using localrequirement.application.dtos;
using localrequirement.application.interfaces;
using Microsoft.AspNetCore.Mvc;

namespace localrequirement.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentController : ControllerBase
    {
        private readonly ILogger<StudentController> _logger;
        private readonly IStudentServices _studentServices;

        public StudentController(ILogger<StudentController> logger,IStudentServices studentServices)
        {
            this._logger = logger;
            this._studentServices = studentServices;
        }
        [HttpGet,HttpGet("GetAll")]
        public async Task<IEnumerable<StudentDto>> Get()
        {
            //throw new Exception("bj");
            return await _studentServices.GetAllAsync();
        }

        [HttpPost,HttpPost("Add")]
        public async Task<StudentDto> Create([FromBody] StudentDto student)
        {
            return await _studentServices.AddAsync(student);
        }
        [HttpPut, HttpPut("update")]
        public async Task<bool> Update([FromBody] StudentDto student)
        {
            return await _studentServices.UpdateAsync(student);
        }
    }
}