﻿using localrequirement.application.dtos;
using localrequirement.application.interfaces;
using localrequirement.domain.entities.identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace localrequirement.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IJWTService _jWTService;
        private readonly IUserService _userService;

        public AuthController(IJWTService jWTService, IUserService userService)
        {
            this._jWTService = jWTService;
            this._userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("authenticate")]
        public async Task<IActionResult> AuthenticateAsync(UserLoginDto user)
        {
            var validUser = await _userService.IsValidUserAsync(user);

            if (!validUser)
            {
                return Unauthorized("Incorrect username or password!");
            }

            var token = _jWTService.GenerateToken(user.Username);

            if (token == null)
            {
                return Unauthorized("Invalid Attempt!");
            }

            // saving refresh token to the db
            RefreshTokenDto obj = new RefreshTokenDto
            {
                Refresh_Token = token.Refresh_Token,
            };

            _userService.AddUserRefreshTokens(obj);
            _userService.SaveCommit();
            return Ok(token);
        }

        //[AllowAnonymous]
        //[HttpPost]
        //[Route("refresh")]
        //public IActionResult Refresh(RefreshTokenDto token)
        //{
        //    var principal = jWTManager.GetPrincipalFromExpiredToken(token.Access_Token);
        //    var username = principal.Identity?.Name;

        //    //retrieve the saved refresh token from database
        //    var savedRefreshToken = userServiceRepository.GetSavedRefreshTokens(username, token.Refresh_Token);

        //    if (savedRefreshToken.RefreshToken != token.Refresh_Token)
        //    {
        //        return Unauthorized("Invalid attempt!");
        //    }

        //    var newJwtToken = jWTManager.GenerateRefreshToken(username);

        //    if (newJwtToken == null)
        //    {
        //        return Unauthorized("Invalid attempt!");
        //    }

        //    // saving refresh token to the db
        //    UserRefreshTokens obj = new UserRefreshTokens
        //    {
        //        RefreshToken = newJwtToken.Refresh_Token,
        //        UserName = username
        //    };

        //    userServiceRepository.DeleteUserRefreshTokens(username, token.Refresh_Token);
        //    userServiceRepository.AddUserRefreshTokens(obj);
        //    userServiceRepository.SaveCommit();

        //    return Ok(newJwtToken);
        //}
    }
}
