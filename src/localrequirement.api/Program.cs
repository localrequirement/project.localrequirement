
using localrequirement.api.middlewares;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddServices(builder.Configuration);

var app = builder.Build();
await app.Services.InitializeServicesAsync();


if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


//dont change calling order
// <group-1>
app.UseMiddleware<ResponseFormmatterMiddleware>(); 
app.UseErrorHandler();
// </group-1>

//app.UseHttpsRedirection();
//app.UseAuthorization();

app.MapControllers();
app.Run();
