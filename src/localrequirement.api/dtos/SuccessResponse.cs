﻿namespace localrequirement.api.dtos
{
    public class SuccessResponse: ResponseBase
    {
        public object Data { get; set; }
    }
}
