﻿using System.Net;

namespace localrequirement.api.dtos
{
    public abstract class ResponseBase
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
