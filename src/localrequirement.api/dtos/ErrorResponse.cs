﻿namespace localrequirement.api.dtos
{
    public class ErrorResponse : ResponseBase
    {
        public List<object> Errors { get; set; }
        public ErrorResponse()
        {
            Errors = new List<object>();
        }
    }
}
