﻿using localrequirement.application.interfaces;

namespace localrequirement.api.middlewares
{
    public class ResponseFormmatterMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IJsonSerializerService _jsonSerializer;

        public ResponseFormmatterMiddleware(RequestDelegate next, 
            IJsonSerializerService jsonSerializer)
        {
            _next = next;
            this._jsonSerializer = jsonSerializer;
        }

        public async Task Invoke(HttpContext context)
        {
            //Fetch original response  
            Stream responseBody = context.Response.Body;

            using (var newResponseBody = new MemoryStream())
            {
                context.Response.Body = newResponseBody;
                await _next(context);

                newResponseBody.Seek(0, SeekOrigin.Begin);
                context.Response.Body = responseBody;
                context.Response.ContentType = "application/json";

                using (var strm = new StreamReader(newResponseBody))
                {
                    string body = strm.ReadToEnd();
                    // if success response (200->299) then only format
                    if (Enumerable.Range(200, 100).Any(t => t == context.Response.StatusCode))
                    {
                        body = await _jsonSerializer.SerializeAsync(new SuccessResponse
                        {
                            Data = (await _jsonSerializer.DeserializeAsync<object>(body)),
                            Status = context.Response.StatusCode
                        });
                    }
                    context.Response.ContentLength = body.Length;
                    // Write mified content to response  
                    await context.Response.WriteAsync(body);
                }
            }
        }
    }

}
