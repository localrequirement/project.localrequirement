﻿using localrequirement.application.interfaces;
using Microsoft.AspNetCore.Http;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace localrequirement.api.middlewares
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IJsonSerializerService _jsonSerializer;

        public ErrorHandlerMiddleware(RequestDelegate next, IJsonSerializerService jsonSerializer)
        {
            _next = next;
            this._jsonSerializer = jsonSerializer;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType =  "application/json";

                var errorResponse = error.DigestException();
                response.StatusCode = errorResponse.Status;

                var result =await _jsonSerializer.SerializeAsync(errorResponse);
                await response.WriteAsync(result);
            }
        }
    }

}
