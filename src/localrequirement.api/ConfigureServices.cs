using localrequirement.api.middlewares;
using localrequirement.application.exceptions;
using localrequirement.infrastructure;

public static class ConfigureServices
{
    internal static IServiceCollection AddServices(this IServiceCollection services,IConfiguration configuration)
    {
        services.AddControllers()
            .AddNewtonsoftJson();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        services.AddInfrastructureServices(configuration);
        services.AddAuthentication(configuration);

        return services;
    }

    internal static async Task InitializeServicesAsync(this IServiceProvider serviceProvider)
    {
        await serviceProvider.InitialiseDatabaseAsync();
    }
    internal static void UseErrorHandler(this IApplicationBuilder app)
    {
        app.UseMiddleware<ErrorHandlerMiddleware>();
        app.UseStatusCodePages(new StatusCodePagesOptions
        {
            HandleAsync = (context) => {
                var request = context.HttpContext.Request;
                var response = context.HttpContext.Response;
                string method = context.HttpContext.Request.Method;
                string url = $"{request.Scheme}://{request.Host}{request.Path}{request.QueryString}";
                int status = response.StatusCode;
                if (context.HttpContext.Response.StatusCode == 404)
                    throw new HttpException($"End point not found: ${request.Path}", status);
                else if (context.HttpContext.Response.StatusCode == 405)
                    throw new HttpException($"Invalid HTTP method: {method?.ToUpper()} ${request.Path}", status);
                else
                    throw new HttpException("Un-handled status code: ", status);
            }
        });
    }
}
