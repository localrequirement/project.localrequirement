﻿using localrequirement.application.exceptions;
using System.Net;

namespace localrequirement.api.extensions
{
    public static class ErrorDigestExtensions
    {
        public static ErrorResponse DigestException(this Exception error)
        {
            var response = new ErrorResponse();
            switch (error)
            {
                case HttpException e:
                    {
                        response.Status = e.StatusCode;
                        response.Message = e.Message;
                        break;
                    }
                case AppException e:
                    {
                        response.Status = e.StatusCode;
                        response.Message = e.Message;
                        break;
                    }
                default:
                    {
                        response.Status = (int)HttpStatusCode.InternalServerError;
                        response.Message = error.Message;
                        break;
                    }
            }
            return response;
        }
    }
}
