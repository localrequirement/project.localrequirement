using System.ComponentModel.DataAnnotations.Schema;

namespace localrequirement.domain.common;
public abstract class BaseEntity 
{
    public DateTime CreatedOn { get; set; }

    public long? CreatedBy { get; set; }

    public DateTime? LastModifiedOn { get; set; }

    public long? LastModifiedBy { get; set; }
    public BaseEntity()
    {
        CreatedOn = DateTime.Now;
    }
}
