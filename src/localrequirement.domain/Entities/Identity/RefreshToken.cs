﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace localrequirement.domain.entities.identity;

public class RefreshToken:BaseEntity
{
    [Key]
    public Guid Id { get; set; }
    [Required]
    public string Refresh_Token { get; set; }
    public bool IsActive { get; set; } = true;
    [ForeignKey("User")]
    public string UserId { get; set; }
    public User User { get; set; }
}