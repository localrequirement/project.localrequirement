
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace localrequirement.domain.entities.identity;

public class User: IdentityUser
{
    
    [StringLength(100)]
    public string Name { get; set; }
    [StringLength(100)]
    public string Password { get; set; }
    public bool IsAnonymous { get; set; }
    
    public ICollection<RefreshToken> RefreshTokens { get; set; }
}
