namespace localrequirement.application.exceptions;

public class AppException : Exception
{
	private int statusCode=500;
	public int StatusCode { get => statusCode; protected set => statusCode = value; }
	public AppException(string message):base(message)
	{

	}
}


public class HttpException : AppException
{
    public HttpException(string message,int status) : base(message)
    {
        base.StatusCode = status;
    }
}
public class BadRequestException:Exception
{
    
}