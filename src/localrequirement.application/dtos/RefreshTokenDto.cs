﻿namespace localrequirement.application.dtos
{
    public class RefreshTokenDto
    {
        public string Refresh_Token { get; set; }
    }

}
