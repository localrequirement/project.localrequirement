﻿namespace localrequirement.application.dtos
{
    public class AuthTokenDto
    {
        public string Access_Token { get; set; }
        public string Refresh_Token { get; set; }
    }
}
