﻿namespace localrequirement.application.interfaces
{
    public interface IJsonSerializerService
    {
        public Task<string> SerializeAsync<T>(T payload);
        public Task<T> DeserializeAsync<T>(string payload);
    }
}
