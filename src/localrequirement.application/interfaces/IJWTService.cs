﻿using System.Security.Claims;

namespace localrequirement.application.interfaces
{
    public interface IJWTService
    {
        AuthTokenDto GenerateToken(string userName);
        AuthTokenDto GenerateRefreshToken(string userName);
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    }
}
