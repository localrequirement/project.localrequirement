﻿namespace localrequirement.application.interfaces
{
    public interface IUserService
    {
        Task<bool> IsValidUserAsync(UserLoginDto user);

        RefreshTokenDto AddUserRefreshTokens(RefreshTokenDto user);

        RefreshTokenDto GetRefreshTokens(string userId, string refreshtoken);

        void DeleteUserRefreshTokens(string userId, string refreshToken);

        int SaveCommit();
    }
}
