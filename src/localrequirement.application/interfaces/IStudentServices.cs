﻿namespace localrequirement.application.interfaces
{
    public interface IStudentServices
    {
        public Task<StudentDto> AddAsync(StudentDto payload);
        public Task<bool> UpdateAsync(StudentDto payload);
        public Task<IEnumerable<StudentDto>> GetAllAsync();
    }
}
