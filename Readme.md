
### About
- ef
- global error handler
- auto response formatter

### Migration Setup
If **dotnet-ef** not extis
```
> dotnet tool install --global dotnet-ef --version 7.*
```

### Migration Setup
```
> cd localrequirement.infrastructure
```

Set Enviroment Variable
```
> SET ASPNETCORE_ENVIRONMENT=Development
```

Add new migration
```
> dotnet-ef migrations add Initial --verbose
```

Add new migration if multiple context
```
> dotnet-ef migrations add Initial --verbose --context=[context class name]
```

Update Database
```
> dotnet-ef database update --verbose [--context=[context class name]]
```




identity links
https://curity.io/resources/learn/jwt-best-practices/
https://codepedia.info/aspnet-core-jwt-refresh-token-authentication
https://codewithmukesh.com/blog/user-management-in-aspnet-core-mvc/